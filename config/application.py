#-*- coding: utf-8 -*-

import os
import yaml

# определяем корень приложения Flask
ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Окружение в которой работает приложение, production, development или test.
# Устанавливается при запуске приложения, через переменную окружения ENV, например export ENV='production'.
# По умолчаниию development.
ENVIROMENT = os.environ.get('ENV', 'development')

# Настройки для подключения базы данных
path_config = os.path.join(ROOT, 'config', 'database.yml')
yaml_config = yaml.load(open(path_config, 'r'))

DATABASE = yaml_config.get(ENVIROMENT).get('database')
HOST     = yaml_config.get(ENVIROMENT).get('host')
PORT     = yaml_config.get(ENVIROMENT).get('port', 5432)
USERNAME = yaml_config.get(ENVIROMENT).get('username')
PASSWORD = yaml_config.get(ENVIROMENT).get('password')

