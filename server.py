
from app.flask import app

# Импортируем вьюхи
from app.views.sessions import *
from app.views.pages import *
from app.views.main import *

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
