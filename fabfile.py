# -*- coding: utf-8 -*-

import re, os, yaml

from datetime import datetime
from fabric.api import *
from app import root

enviroment = 'production'

path_config = os.path.join(root, 'config', 'deploy.yml')
yaml_config = yaml.load(open(path_config, 'r'))

env['repository']    = yaml_config.get(enviroment).get('repo')
env['hosts']         = yaml_config.get(enviroment).get('servers')
env['user']          = yaml_config.get(enviroment).get('user')
env['path']          = yaml_config.get(enviroment).get('path')
env['shared_dirs']   = yaml_config.get(enviroment).get('shared_dirs',  [])
env['level']         = yaml_config.get(enviroment).get('level',        'aborts')
env['release_count'] = yaml_config.get(enviroment).get('release_count', 5      )
env['packages']      = yaml_config.get(enviroment).get('packages',      []      )

env['release_path'] = env['path'] + '/releases/' + datetime.now().strftime("%Y%m%d%H%M%S")

def rm():
    "Удаляем проект"
    run("rm -rf %s" % env['path'])

def init():
    print env['hosts']
    packages = ' '.join(env['packages'])
    print packages
    sudo('sudo apt-get install %s' % packages)

    # содаем структуру папок
    run ('mkdir -p %s/releases' % (env['path']))
    run ('mkdir -p %s/shared' % (env['path']))

    # настраиваем виртуальное окружение
    sudo('pip install virtualenv')
    run('virtualenv %s/shared/venv' % env['path'])

    # создаем папки для шаринга
    for f in env['shared_dirs']:
        run ('mkdir -p %s/shared/%s' % (env['path'], f))

def deploy():

    # создаем папку с релизом
    run ('mkdir -p %s' % env['release_path'])

    # клоним проект
    run('git clone %s %s' % (env['repository'], env['release_path']))

    # линкуем папки(проверяем наличае, если нет создает)
    for f in env['shared_dirs']:
        run ('ln -s %s/shared/%s %s/%s' % (env['path'], f, env['release_path'], f))

    # ставим плагины
    run('%s/venv/bin/pip install -r %s/requirements' % (env['release_path'], env['release_path']))

    # линкуем current папку на релиз
    run('rm -rf %s/current' % env['path'])
    run('ln -s %s %s/current' % (env['release_path'], env['path']))

    # перезапускаем uwsgi
    run('touch %s/uwsgi.yaml' % env['release_path'])
    # удаляем старые релизы
    release_count = run('ls -1 %s/releases  | wc -l' % env['path'])
    if int(release_count) > env['release_count']:
        old_releases = run('ls -tr %s/releases' % env['path'])
        old_releases = re.split('[\t\r\n]*', old_releases)
        old_releases.sort(reverse=True)
        for release in old_releases[env['release_count']:]:
            run("rm -rf %s/releases/%s" % (env['path'], release))