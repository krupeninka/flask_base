import sys
import datetime

from app.db import db
from app.models.user import User

drop = [User]
create = [User]

for model in drop:
    try:
        model.drop_table()
        print("Модель {} удалена".format(model))
    except Exception as ex:
        print("Ошибка удаления модели {}".format(model))
        print(ex)


print()
for model in create:
    model.create_table()
    print("Модель {} создана".format(model))

print()
print("Добавление пользователей")
krupenin, _ = User.get_or_create(username = 'krupenin', defaults = { 'password': '123456aA',
    'name': 'Константин Крупенин', 'role': 'admin' })
