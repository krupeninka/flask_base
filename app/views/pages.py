from flask import render_template

from app.flask import app
from app.models.user import User


@app.route('/pages')
@app.route('/pages/dashboard')
def pages_dashboard():
    return render_template('pages/dashboard.haml')


@app.route('/pages/flot')
def pages_flot():
    return render_template('pages/flot.haml')


@app.route('/pages/tables')
def pages_tables():
    return render_template('pages/tables.haml')


@app.route('/pages/forms')
def pages_forms():
    return render_template('pages/forms.haml')


@app.route('/pages/panels-wells')
def pages_panels_wells():
    return render_template('pages/panels_wells.haml')


@app.route('/pages/buttons')
def pages_buttons():
    return render_template('pages/buttons.haml')


@app.route('/pages/notifications')
def pages_notifications():
    return render_template('pages/notifications.haml')


@app.route('/pages/icons')
def pages_icons():
    return render_template('pages/icons.haml')


@app.route('/pages/blank')
def pages_blank():
    return render_template('pages/blank.haml')


@app.route('/pages/login')
def pages_login():
    return render_template('pages/login.haml')
